package model.data_structures;

public interface IList <T> extends Iterable<T> {
	 T darPrimero();

	 T darElemento();
	 
	 T eliminar(T dato);
	 
	 public void agregar(T dato);
	 
	 T buscar(T dato);
	

}
